# TP 5 - Deployment

```bash
mkdir ../TP05 && cd ../TP05
kubectl create deployment my-app --image=samiche92/my-node-server:1.0.0 -o yaml --dry-run=client > my-app.yaml
vim my-app.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - image: samiche92/my-node-server:1.0.0
        name: my-node-server
```

```bash
kubectl delete all --all
kubectl apply -f my-app.yaml
kubectl get pods -o jsonpath="{.items[*].spec.containers[*].image}"
kubectl describe deploy my-app | grep -i image
kubectl rollout history deployment my-app

vim my-app.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
  annotations:
    kubernetes.io/change-cause: "v2"
[...]
    spec:
      containers:
      - image: samiche92/my-node-server:2.0.0
        name: my-node-server
```

```bash
kubectl get pods -w
# Dans un 2nd terminal
kubectl apply -f my-app.yaml
kubectl rollout history deployment my-app

kubectl get rs -w
# Dans un 2nd terminal
kubectl get pods -w
# Dans un 3e terminal
kubectl rollout undo deployment my-app --to-revision=1

kubectl describe deploy my-app
kubectl get deploy my-app -o yaml

kubectl explain deployment
kubectl explain deployment.spec
kubectl explain deployment.spec.strategy
kubectl explain deployment.spec.strategy.rollingUpdate
```

