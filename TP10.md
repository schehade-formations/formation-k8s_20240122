# TP 10 - ConfigMap, Secret

```bash
mkdir ../TP10 && cd ../TP10
kubectl create configmap my-cm --from-literal=username=toto
kubectl get configmaps
kubectl get cm my-cm -o yaml
vim my-file.conf
```

```
conf1=test1
conf2=test2
```

```bash
kubectl create configmap my-cm --from-file=my-file.conf -o yaml --dry-run=client
kubectl create configmap my-cm --from-file=app1.conf=my-file.conf -o yaml --dry-run=client
mkdir confs
vim confs/cf1.conf
vim confs/cf2.conf

cp ../TP07/my-pod.yaml .
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    command:
    - sh
    - -c
    args:
    - while true; do date; sleep $(DURATION); done
    env:
    - name: DURATION
      valueFrom:
        configMapKeyRef:
          name: duration-sylvain
          key: interval1
```

```bash
kubectl create configmap duration --from-literal=interval=5s
kubectl get cm
kubectl get cm duration -o yaml
kubectl apply -f my-pod.yaml

kubectl create configmap cm-test-1 --from-file=my-file.conf
kubectl get cm cm-test-1 -o yaml
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  volumes:
  - name: my-vol
    configMap:
      name: cm-test-1
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    volumeMounts:
    - name: cm-test-1
      mountPath: /tmp/confs
```

```bash
kubectl delete -f my-pod.yaml
kubectl apply -f my-pod.yaml
kubectl get pods
kubectl get cm cm-test-1 -o yaml
kubectl exec my-pod -- ls /tmp/confs
kubectl exec my-pod -- cat /tmp/confs/my-file.conf
kubectl exec -it my-pod -- bash
cd /tmp/confs/
cat my-file.conf 
echo "conf3=test3" >> my-file.conf 
exit

cp ../TP01/app.js .
vim app.js
# www.listen(8080); => www.listen(8081);
kubectl create configmap node-app-file --from-file=app.js
kubectl get cm
kubectl describe cm node-app-file
vim my-pod
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  volumes:
  - name: my-vol
    configMap:
      name: node-app-file
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn-1
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn-2
    volumeMounts:
    - name: my-vol
      mountPath: /app.js
      subPath: app.js
```

```bash
kubectl delete -f my-pod.yaml
kubectl apply -f my-pod.yaml
kubectl exec my-pod -c my-ctn-1 -- curl localhost:8080
kubectl exec my-pod -c my-ctn-1 -- curl localhost:8081
```

## Secret

```bash
kubectl create secret generic my-sct --from-literal=pwd=mypass
 kubectl get secret
kubectl describe secret my-sct
kubectl get secret my-sct -o yaml
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  volumes:
  - name: my-vol-cm
    configMap:
      name: node-app-file
  - name: my-vol-sct
    secret:
      secretName: my-sct
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn-1
  - image: samiche92/my-node-server:1.0.0
    name: my-ctn-2
    volumeMounts:
    - name: my-vol-cm
      mountPath: /app.js
      subPath: app.js
    - name: my-vol-sct
      mountPath: /tmp/secret.txt
      subPath: pwd
```

```bash
kubectl delete -f my-pod.yaml
kubectl apply -f my-pod.yaml
kubectl exec my-pod -c my-ctn-2 -- ls /tmp
kubectl exec my-pod -c my-ctn-2 -- cat /tmp/secret.txt
```

