# TP 8 - Liveness Probe, ReadinessProbe

```bash
kubectl delete all --all
mkdir ../TP08 && cd ../TP08
cp ../TP07/my-pod.yaml .
vim my-pod.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    command:
    - sh
    - -c
    args:
    - while true; do date; sleep 1s; done
    livenessProbe:
      exec:
        command:
        - cat
        - /app.js
```

```bash
kubectl delete pod --all
kubectl apply -f my-pod.yaml
kubectl exec my-pod -- ls
kubectl get events -w
# Dans un 2nd terminal
kubectl get pods -w
# Dans un 3e terminal
kubectl exec my-pod -- rm /app.js
# On attend que le pod redemarre
kubectl exec my-pod -- ls /

# Readiness probe

kubectl delete pod --all
vim my-app.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod-1
  labels:
    app: backend
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
---
apiVersion: v1
kind: Pod
metadata:
  name: my-pod-2
  labels:
    app: backend
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
---
apiVersion: v1
kind: Pod
metadata:
  name: my-pod-3
  labels:
    app: backend
spec:
  containers:
  - image: samiche92/my-node-server:3.0.0
    name: my-pod
---
apiVersion: v1
kind: Pod
metadata:
  name: test
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
---
apiVersion: v1
kind: Service
metadata:
  name: my-svc
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 8080
  selector:
    app: backend
  type: ClusterIP
```

```bash
kubectl apply -f my-app.yaml
kubectl get endpoints
kubectl exec -it test -- bash
while true; do curl my-svc; sleep 1s; done
exit
vim my-app.yaml
```

```yaml
[...]
  - image: samiche92/my-node-server:3.0.0
    name: my-pod
    readinessProbe:
      httpGet:
        path: /
        port: 8080
[...]
```

```bash
kubectl delete -f my-app.yaml
kubectl apply -f my-app.yaml
kubectl exec -it test bash
while true; do curl my-svc; sleep 2s; done
# Dans un 2nd terminal qd on remarque que my-pod-3 ne repond plus
kubectl get pods
kubectl get pods -o wide
# Dans le terminal de test
curl <@IP-my-pod-3>:8080/repair
while true; do curl my-svc; sleep 2s; done
```
