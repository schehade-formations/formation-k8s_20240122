# TP 1 - Docker

```bash
mkdir TP01 && cd TP01
vim app.js
```

```javascript
const http = require('http');
const os = require('os');

console.log("Server v1 starting...");

var handler = function(request, response) {
  console.log("Received request from " + request.connection.remoteAddress);
  response.writeHead(200);
  response.end("You've hit " + os.hostname() + "\n");
};

var www = http.createServer(handler);
www.listen(8080);
```

```bash
vim Dockerfile
```

```
FROM node:7

ADD app.js /app.js

ENTRYPOINT ["node", "app.js"]
```

```bash
docker build -t my-node-server:1.0.0 .
docker images
docker run --name my-ctn -d my-node-server:1.0.0
docker ps
docker logs my-ctn
docker exec -it my-ctn bash
ps aux
ls /
curl localhost:8080
exit

# Se creer un compte sur https://hub.docker.com/ ou se connecter si deja existant
docker login
docker tag my-node-server:1.0.0 samiche92/my-node-server:1.0.0
docker images
docker push samiche92/my-node-server:1.0.0
```

