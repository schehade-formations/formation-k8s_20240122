# TP 13 - StatefulSet

```bash
mkdir ../TP13 && cd ../TP13
cp ../TP09/my-pvc.yaml .
cp ../TP04/my-rs.yaml .
vim my-rs.yaml
```

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: my-rs
spec:
  replicas: 3
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
        env: dev
    spec:
      volumes:
      - name: my-vol
        persistentVolumeClaim:
          claimName: my-pvc
      containers:
      - name: my-ctn
        image: samiche92/my-node-server:2.0.0
        ports:
        - containerPort: 8080
        volumeMounts:
        - mountPath: /cache2
          name: my-vol
```

```bash
kubectl delete all --all
kubectl get pvc,pv
kubectl apply -f my-pvc.yaml
kubectl get pvc,pv
kubectl apply -f my-rs.yaml
kubectl get rs,pod,pvc

mkdir my-app
vim my-app/my-svc.yaml
```

```bash
apiVersion: v1
kind: Service
metadata:
  name: my-svc
spec:
  clusterIP: None
  ports:
  - port: 80
    protocol: TCP
    targetPort: 8080
  selector:
    app: backend
  type: ClusterIP
```

```
vim my-app/my-ss.yaml
```

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: my-ss
spec:
  selector:
    matchLabels:
      app: backend
  serviceName: "my-svc"
  replicas: 3
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - name: my-ctn
        image: samiche92/my-node-server:1.0.0
        volumeMounts:
        - name: my-vol
          mountPath: /tmp/data
  volumeClaimTemplates:
  - metadata:
      name: my-vol
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: "standard"
      resources:
        requests:
          storage: 1Gi
```

```bash
kubectl delete rs --all
kubectl get pods -w
kubectl apply -f my-app/
kubectl get pvc,pv
kubectl delete limitrange --all
kubectl delete resourcequota --all
vim my-app/my-ss.yaml
# replicas: 3 => 4
kubectl apply -f my-app/
kubectl get pods,pvc,pv
vim my-app/my-ss.yaml
# replicas: 4 => 2
kubectl apply -f my-app/
kubectl get pods,pvc,pv
kubectl get svc
kubectl get endpoints
kubectl exec my-ss-0 -- curl localhost:8080
kubectl get pods -o wide
kubectl exec my-ss-0 -- curl 10.244.0.7:8080
kubectl exec my-ss-0 -- curl my-ss-1.my-svc.default.svc.cluster.local:8080
kubectl exec my-ss-0 -- curl my-ss-1.my-svc.default:8080
kubectl exec my-ss-0 -- curl my-ss-1.my-svc:8080
```
