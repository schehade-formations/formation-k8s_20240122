# TP - Install cluster K8s

Test fait sur une machine avec Ubuntu 20.04.

1. Installer Docker  
https://docs.docker.com/engine/install/ubuntu/

2. Installer kubeadm  
https://kubernetes.io/fr/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/

3. Creer un nouveau cluster avec Calico  
https://docs.tigera.io/calico/latest/getting-started/kubernetes/quickstart

! Regler le potentiel soucis au moment de "kubeadm init"  
https://forum.linuxfoundation.org/discussion/862825/kubeadm-init-error-cri-v1-runtime-api-is-not-implemented

