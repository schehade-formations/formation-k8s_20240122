# TP - Helm

Installer Helm  
https://helm.sh/docs/intro/install/

```bash
helm create project-test
helm install project-test/ --generate-name
helm ls
kubectl get deploy,rs,pod,ingress
helm uninstall project-test-1706109603
vim project-test/templates/deployment.yaml 
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.projectName }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ .Values.projectDeployMatchLabel }}
  template:
    metadata:
      labels:
        app: {{ .Values.projectDeployMatchLabel }}
    spec:
      containers:
        - name: my-ctn
          image: {{ .Values.image.repository }}:{{ .Values.image.tag }}
```

```bash
cd project-test/templates/
rm -r _helpers.tpl hpa.yaml ingress.yaml serviceaccount.yaml service.yaml tests/
cd -
tree project-test/
vim project-test/values.yaml
```

```yaml
projectName: my-app
replicaCount: 2
projectDeployMatchLabel: backend
image:
  repository: samiche92/my-node-server
  tag: 1.0.0
```

```bash
vim project-test/templates/NOTES.txt 
# My test app v1.0.0
helm install project-test/ --generate-name
kubectl get deploy,rs,pod,ingress
helm uninstall project-test-1706110431
vim project-test/values-dev.yaml 
```

```yaml
replicaCount: 1
image:
  tag: 2.0.0
```

```bash
helm install -f project-test/values-dev.yaml project-test project-test/
helm ls
kubectl get deploy,rs,pod,ingress
kubectl describe pod my-app-5fdb865c7b-dpm9z | grep -i image
helm uninstall project-test
helm install -f project-test/values-dev.yaml project-test project-test/ --dry-run --debug
```

